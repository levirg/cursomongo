'use strict'

var express = require('express');
var frutaCtrl = require('../controllers/frutaCtrl');

var api = express.Router();

api.post('/fruta', frutaCtrl.saveFruta);
api.get('/frutas', frutaCtrl.getFrutas);
api.get('/fruta/:id', frutaCtrl.getFruta);
api.put('/fruta/:id', frutaCtrl.updateFruta);
api.delete('/fruta/:id', frutaCtrl.deleteFruta);
module.exports = api;
